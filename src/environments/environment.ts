// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD_NSmor7D1WWrOmi_oSS2sxS6yqsDcCEI",
    authDomain: "finalproject-jce.firebaseapp.com",
    projectId: "finalproject-jce",
    storageBucket: "finalproject-jce.appspot.com",
    messagingSenderId: "931312769245",
    appId: "1:931312769245:web:eb84fdc06794e52b152334"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
