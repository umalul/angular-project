export interface CovidRaw {
        ID:string
        Message: string
        Countries: [
          {
            ID: string
            Country: string
            CountryCode: string
            Slug: string
            NewConfirmed: number
            TotalConfirmed: number
            NewDeaths: number
            TotalDeaths: number
            NewRecovered: number
            TotalRecovered: number
            Date: Date
          }
        ],
        Date: Date
}
